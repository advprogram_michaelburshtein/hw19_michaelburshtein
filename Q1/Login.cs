﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace Q1
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }


        private void LoginBtn_Click(object sender, EventArgs e)
        {
            // Variables
            string formatLogin = UserNameTB.Text + "," + PasswordTB.Text;
            FileStream fs = new FileStream("Users.txt", FileMode.OpenOrCreate, FileAccess.Read);
            StreamReader userDB = new StreamReader(fs);
            string loginLine;
            bool foundUser = false;
            // Seeking for the user
            while(!foundUser && (loginLine = userDB.ReadLine()) != null)
            {
                foundUser = formatLogin == loginLine;
            }

            // Checking if user was found
            if(foundUser)
            {
                // Displaying the birthday calander
                this.Hide();
                Form calanderForm = new Birthdays(UserNameTB.Text);
                calanderForm.Show();
            }
            else
            {
                // Displaying error message to user
                MessageBox.Show("Wrong username or password, user not found.", "User Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            fs.Close();
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            
            Environment.Exit(0);
        }

        private void CheckEnter(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                LoginBtn.PerformClick();
            }
        }
    }
}

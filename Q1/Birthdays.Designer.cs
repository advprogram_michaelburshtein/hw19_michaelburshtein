﻿namespace Q1
{
    partial class Birthdays
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BirthdayCalander = new System.Windows.Forms.MonthCalendar();
            this.CurBirthdayLbl = new System.Windows.Forms.Label();
            this.SelectionDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.NameTxtBox = new System.Windows.Forms.TextBox();
            this.AddBirthdayBtn = new System.Windows.Forms.Button();
            this.EventSelection = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // BirthdayCalander
            // 
            this.BirthdayCalander.FirstDayOfWeek = System.Windows.Forms.Day.Sunday;
            this.BirthdayCalander.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.BirthdayCalander.Location = new System.Drawing.Point(18, 18);
            this.BirthdayCalander.MaxSelectionCount = 1;
            this.BirthdayCalander.Name = "BirthdayCalander";
            this.BirthdayCalander.TabIndex = 0;
            this.BirthdayCalander.TitleBackColor = System.Drawing.Color.DarkGoldenrod;
            this.BirthdayCalander.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.BirthdayCalander_DateSelected);
            // 
            // CurBirthdayLbl
            // 
            this.CurBirthdayLbl.AutoSize = true;
            this.CurBirthdayLbl.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.CurBirthdayLbl.Location = new System.Drawing.Point(18, 193);
            this.CurBirthdayLbl.Name = "CurBirthdayLbl";
            this.CurBirthdayLbl.Size = new System.Drawing.Size(0, 17);
            this.CurBirthdayLbl.TabIndex = 1;
            // 
            // SelectionDate
            // 
            this.SelectionDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.SelectionDate.Location = new System.Drawing.Point(255, 69);
            this.SelectionDate.Name = "SelectionDate";
            this.SelectionDate.Size = new System.Drawing.Size(203, 22);
            this.SelectionDate.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(257, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 19);
            this.label1.TabIndex = 3;
            this.label1.Text = "Add Event:";
            // 
            // NameTxtBox
            // 
            this.NameTxtBox.Font = new System.Drawing.Font("Consolas", 9.75F);
            this.NameTxtBox.ForeColor = System.Drawing.Color.LightGray;
            this.NameTxtBox.Location = new System.Drawing.Point(255, 40);
            this.NameTxtBox.Name = "NameTxtBox";
            this.NameTxtBox.Size = new System.Drawing.Size(203, 23);
            this.NameTxtBox.TabIndex = 1;
            this.NameTxtBox.Text = "Birthday Boy/Girl Name";
            this.NameTxtBox.Enter += new System.EventHandler(this.NameTxtBox_Enter);
            this.NameTxtBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NameTxtBox_KeyDown);
            this.NameTxtBox.Leave += new System.EventHandler(this.NameTxtBox_Leave);
            // 
            // AddBirthdayBtn
            // 
            this.AddBirthdayBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.AddBirthdayBtn.Location = new System.Drawing.Point(255, 151);
            this.AddBirthdayBtn.Name = "AddBirthdayBtn";
            this.AddBirthdayBtn.Size = new System.Drawing.Size(203, 29);
            this.AddBirthdayBtn.TabIndex = 4;
            this.AddBirthdayBtn.Text = "Add Event";
            this.AddBirthdayBtn.UseVisualStyleBackColor = true;
            this.AddBirthdayBtn.Click += new System.EventHandler(this.AddBirthdayBtn_Click);
            // 
            // EventSelection
            // 
            this.EventSelection.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EventSelection.ForeColor = System.Drawing.Color.LightGray;
            this.EventSelection.FormattingEnabled = true;
            this.EventSelection.Items.AddRange(new object[] {
            "Birthday"});
            this.EventSelection.Location = new System.Drawing.Point(255, 97);
            this.EventSelection.Name = "EventSelection";
            this.EventSelection.Size = new System.Drawing.Size(203, 23);
            this.EventSelection.TabIndex = 3;
            this.EventSelection.Text = "Event\'s Name";
            this.EventSelection.Enter += new System.EventHandler(this.EventSelection_Enter);
            this.EventSelection.KeyDown += new System.Windows.Forms.KeyEventHandler(this.EventSelection_KeyDown);
            this.EventSelection.Leave += new System.EventHandler(this.EventSelection_Leave);
            // 
            // Birthdays
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(470, 201);
            this.Controls.Add(this.EventSelection);
            this.Controls.Add(this.AddBirthdayBtn);
            this.Controls.Add(this.NameTxtBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SelectionDate);
            this.Controls.Add(this.CurBirthdayLbl);
            this.Controls.Add(this.BirthdayCalander);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(486, 2000);
            this.MinimizeBox = false;
            this.Name = "Birthdays";
            this.ShowIcon = false;
            this.Text = "Birthdays";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Birthdays_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar BirthdayCalander;
        private System.Windows.Forms.Label CurBirthdayLbl;
        private System.Windows.Forms.DateTimePicker SelectionDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox NameTxtBox;
        private System.Windows.Forms.Button AddBirthdayBtn;
        private System.Windows.Forms.ComboBox EventSelection;
    }
}
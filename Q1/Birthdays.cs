﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

/* Database format: [NAME],[MM]/[DD]/[YYYY],[EVENT]
 * Special events: Birthday */

namespace Q1
{
    public partial class Birthdays : Form
    {
        private string username;
        private Dictionary<string, List<string[]>> usersDates; // key: day/month value: [0] - name, [1] - year
        public Birthdays(string username)
        {
            InitializeComponent();
            
            this.username = username;
            // Getting user information from according file
            FileStream userData = new FileStream(username + "BD.xml", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            this.usersDates = new Dictionary<string, List<string[]>>();
            // Trying to read xml file - exception will be thrown if file was created or not in format
            try
            {
                CreateDictFromFile(new StreamReader(userData));
            }
            catch // resetting the file to xml used format
            {
                // Re-opening the file in write mode
                userData.Close();
                userData = new FileStream(username + "BD.xml", FileMode.OpenOrCreate, FileAccess.Write);
                ResetXmlFile(userData); // restting the file
            }
            
            userData.Close();
        }

        private void CreateDictFromFile(StreamReader file)
        {
            XmlReader xmlRead = XmlReader.Create(file);
            while (xmlRead.Read())
            {
                if (xmlRead.AttributeCount == 3)
                {
                    this.AddDateToDictionary(xmlRead);
                }
            }
        }

        /* Place Holder */
        private void NameTxtBox_Leave(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(NameTxtBox.Text))
            {
                NameTxtBox.ForeColor = Color.LightGray;
                NameTxtBox.Text = "Birthday Boy/Girl Name";
            }
        }

        private void NameTxtBox_Enter(object sender, EventArgs e)
        {
            if(NameTxtBox.ForeColor == Color.LightGray)
            {
                NameTxtBox.ForeColor = Color.Black;
                NameTxtBox.Text = "";
            }
        }

        private void EventSelection_Enter(object sender, EventArgs e)
        {
            if(EventSelection.ForeColor == Color.LightGray)
            {
                EventSelection.ForeColor = Color.Black;
                EventSelection.Text = "";
            }
        }

        private void EventSelection_Leave(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(EventSelection.Text))
            {
                EventSelection.ForeColor = Color.LightGray;
                EventSelection.Text = "Event's Name";
            }
        }
        /* Place Holder End */

        private void BirthdayCalander_DateSelected(object sender, DateRangeEventArgs e)
        {
            // Variables
            string selectedDate = String.Format("{0:00}/{1:00}", e.End.Month, e.End.Day);
            if(this.usersDates.ContainsKey(selectedDate))
            {
                CurBirthdayLbl.Text = "";
                // Checking all events on that date
                for (int i = 0; i < this.usersDates[selectedDate].Count; i++)
                {
                    // Printing message according to event name
                    if (this.usersDates[selectedDate][i][2] == "Birthday")
                    {
                        CurBirthdayLbl.Text += this.usersDates[selectedDate][i][0] + " is celebrating " + (DateTime.Now.Year - int.Parse(this.usersDates[selectedDate][i][1])) + " years!\n";
                    }
                    else if(this.usersDates[selectedDate][i][1] == BirthdayCalander.SelectionEnd.Year.ToString()) // Checking if event is in same year as selected year
                    {
                        CurBirthdayLbl.Text += this.usersDates[selectedDate][i][0] + " is going to a " + this.usersDates[selectedDate][i][2] + '\n';
                    }
                }
            }
            else
            {
                CurBirthdayLbl.Text = "No event was found in this date.";
            }
            this.Height = 240 + CurBirthdayLbl.Height;
        }

        private void Birthdays_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }

        

        private void AddBirthdayBtn_Click(object sender, EventArgs e)
        {
            // Opening file
            FileStream fs = new FileStream(this.username + "BD.xml", FileMode.OpenOrCreate, FileAccess.Read);
            
            string eventName = EventSelection.Text;
            // Xml handling variables
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode rootData, data;
            XmlAttribute xmlAtr;
            try
            {
                xmlDoc.Load(fs);
            }
            catch
            {
                // Re-opening the file and resetting it
                fs.Close();
                fs = new FileStream(username + "BD.xml", FileMode.OpenOrCreate, FileAccess.Write);
                ResetXmlFile(fs);
                // Re-opening file normally
                fs.Close();
                fs = new FileStream(username + "BD.xml", FileMode.OpenOrCreate, FileAccess.Read);
                xmlDoc.Load(fs); // Re-loading the document
            }
            // Adding string to date
            string date = String.Format("{0:00}/{1:00}/{2:00}", SelectionDate.Value.Month, SelectionDate.Value.Day, SelectionDate.Value.Year);
            
            // Editing xml file
            rootData = xmlDoc.SelectSingleNode("events");
            data = xmlDoc.CreateElement("event");
            // adding name
            xmlAtr = xmlDoc.CreateAttribute("name");
            xmlAtr.Value = NameTxtBox.Text;
            data.Attributes.Append(xmlAtr);
            // adding date
            xmlAtr = xmlDoc.CreateAttribute("date");
            xmlAtr.Value = date;
            data.Attributes.Append(xmlAtr);
            // adding eventName
            xmlAtr = xmlDoc.CreateAttribute("eventName");
            xmlAtr.Value = eventName;
            data.Attributes.Append(xmlAtr);
            
            rootData.AppendChild(data); // Adding new node to events
            // closing file and rewriting it with new event
            fs.Close();
            fs = new FileStream(this.username + "BD.xml", FileMode.OpenOrCreate, FileAccess.Write);
            xmlDoc.Save(fs);
            fs.Close(); // closing file

            // Adding date to current proram dictionary
            this.AddDateToDictionary(String.Format("{0},{1},{2}", NameTxtBox.Text, date, eventName));
            MessageBox.Show(NameTxtBox.Text + " was added at " + date); // Showing user
        }

        private void NameTxtBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                AddBirthdayBtn.PerformClick();
            }
        }
        private void EventSelection_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.KeyCode == Keys.Return)
            {
                AddBirthdayBtn.PerformClick();
            }
        }

        /// <summary>
        /// Adds the date from the given file line to the main dictionary
        /// </summary>
        /// <param name="fileLine">the file line containing the date information</param>
        private void AddDateToDictionary(string fileLine)
        {
            // Getting all data for dictionary adding
            string[] birthdate = fileLine.Split(','); // getting both strings seperately
            string[] splitDate = birthdate[1].Split('/');
            string dayMonth = splitDate[0] + "/" + splitDate[1];
            string year = splitDate[2];
            string eventName = birthdate[2];
            List<string[]> people;
            if (usersDates.ContainsKey(dayMonth))
            {
                people = usersDates[dayMonth]; // getting the people list for the given date
            }
            else // creating another date in the dictionary
            {
                // Adding user to dictionary
                people = new List<string[]>();
                usersDates.Add(dayMonth, people); // Adding date to dictionary
            }
            people.Add(new string[] { birthdate[0], year, eventName }); // Adding new person to date
        }

        /// <summary>
        /// Adds the date from the given xml file node to the main dictionary
        /// </summary>
        /// <param name="fileNode">the file node containing the date information</param>
        private void AddDateToDictionary(XmlReader fileNode)
        {
            // Getting all data for dictionary adding
            string[] splitDate = fileNode.GetAttribute("date").Split('/');
            string dayMonth = splitDate[0] + "/" + splitDate[1];
            string year = splitDate[2];
            List<string[]> people;
            if (usersDates.ContainsKey(dayMonth))
            {
                people = usersDates[dayMonth]; // getting the people list for the given date
            }
            else // creating another date in the dictionary
            {
                // Adding user to dictionary
                people = new List<string[]>();
                usersDates.Add(dayMonth, people); // Adding date to dictionary
            }
            people.Add(new string[] { fileNode.GetAttribute("name"), year, fileNode.GetAttribute("eventName") }); // Adding new person to date
        }
        
        /// <summary>
        /// Resetting xml file to used format
        /// </summary>
        /// <param name="fs">the xml file opened in write mode</param>
        private void ResetXmlFile(FileStream fs)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode root = xmlDoc.CreateElement("events");
            xmlDoc.AppendChild(root);
            xmlDoc.Save(fs);
        }
    }
}